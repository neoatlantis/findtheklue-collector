function updateStatus(){
    $.ajax({
        url: "/status",
        method: "GET",
        dataType: "json",
    })
    .done(function(j){
        $("#status-token")
            .toggleClass("error", !j.tokenValid)
            .text((j.tokenValid ? "Valid" : "Invalid"));
        $("#status-update").text((new Date().getTime() / 1000.0 - j.lastUpdate).toFixed(0));
        $("#status-silent")
            .toggleClass("error", j.silent)
            .text(j.silent ? "Yes" : "No");
    })
    .always(function(){
        setTimeout(updateStatus, 5000);
    });
}
updateStatus();



function dumpAuthToken(authResult){
    var response = authResult.currentUser.get().getAuthResponse();
    console.log("resp", response);
    console.log("token", response.access_token);

    $.ajax({
        data: JSON.stringify([response.token_type, response.access_token]),
        url: "/token",
        method: "POST",
    });
}

function report(data){
    $.ajax({
        data: JSON.stringify(data),
        url: "/report",
        method: "POST",
    });
}



function generateGplusEntryHTML(data){
    var ret = $('<div>');
    ret.append($('<a>').text(data.title).attr('href', data.url));

    return ret.html();
}

var searchJob = false;
var auth2 = {};
var helper = (function() {
  return {
    /**
     * Hides the sign in button and starts the post-authorization operations.
     *
     * @param {Object} authResult An Object which contains the access token and
     *   other authentication information.
     */
    onSignInCallback: function(authResult) {
      if (authResult.isSignedIn.get()) {
            $('#authOps').show('slow');
            $('#gConnect').hide();
            dumpAuthToken(authResult);
            helper.profile();
            //helper.search();
      } else {
          if (authResult['error'] || authResult.currentUser.get().getAuthResponse() == null) {
            // There was an error, which means the user is not signed in.
            // As an example, you can handle by writing to the console:
            console.log('There was an error: ' + authResult['error']);
          }
          $('#authResult').append('Logged out');
          $('#authOps').hide('slow');
          $('#gConnect').show();
      }

      console.log('authResult', authResult);
    },

    /**
     * Calls the OAuth2 endpoint to disconnect the app for the user.
     */
    disconnect: function() {
      // Revoke the access token.
      searchJob = false;
      auth2.disconnect();
    },

    /**
     * Gets and renders the list of people visible to this app.
     */
    search: function(){
      var keywords = ["#findtheklue", "#NL1331"];

      function doSearch(){
          if(!searchJob) return;
          $('#searchResult').empty();
          for(var i in keywords){
              gapi.client.plus.activities.search({
                'query': keywords[i],
                'maxResults': 10,
              }).then(function(res) {
                  console.log(res);
                  var str = "";
                  for(var i in res.result.items){
                      report(res.result.items[i]);
                      var html = generateGplusEntryHTML(res.result.items[i]);
                      $('<div>').html(html).appendTo('#searchResult');
                      $('<hr>').appendTo('#searchResult');
                  }
              });
          }
          setTimeout(doSearch, 120000);
      }
      searchJob = true;
      doSearch();
    },

    /**
     * Gets and renders the currently signed in user's profile data.
     */
    profile: function(){
      gapi.client.plus.people.get({
        'userId': 'me'
      }).then(function(res) {
        var profile = res.result;
        console.log(profile);
        $('#profile').empty();
        $('#profile').append(
            $('<p><img src=\"' + profile.image.url + '\"></p>'));
        $('#profile').append(
            $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
            profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
        if (profile.emails) {
          $('#profile').append('<br/>Emails: ');
          for (var i=0; i < profile.emails.length; i++){
            $('#profile').append(profile.emails[i].value).append(' ');
          }
          $('#profile').append('<br/>');
        }
        if (profile.cover && profile.coverPhoto) {
          $('#profile').append(
              $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
        }
      }, function(err) {
        var error = err.result;
        $('#profile').empty();
        $('#profile').append(error.message);
      });
    }
  };
})();

/**
 * jQuery initialization
 */
$(document).ready(function() {
  $('#disconnect').click(helper.disconnect);
  $('#loaderror').hide();
  if ($('meta')[0].content == 'YOUR_CLIENT_ID') {
    alert('This sample requires your OAuth credentials (client ID) ' +
        'from the Google APIs console:\n' +
        '    https://code.google.com/apis/console/#:access\n\n' +
        'Find and replace YOUR_CLIENT_ID with your client ID.'
    );
  }
});

/**
 * Handler for when the sign-in state changes.
 *
 * @param {boolean} isSignedIn The new signed in state.
 */
var updateSignIn = function() {
  console.log('update sign in state');
  if (auth2.isSignedIn.get()) {
    console.log('signed in');
    helper.onSignInCallback(gapi.auth2.getAuthInstance());
  }else{
    console.log('signed out');
    helper.onSignInCallback(gapi.auth2.getAuthInstance());
  }
}

/**
 * This method sets up the sign-in listener after the client library loads.
 */
function startApp() {
  gapi.load('auth2', function() {
    gapi.client.load('plus','v1').then(function() {
      gapi.signin2.render('signin-button', {
          scope: 'https://www.googleapis.com/auth/plus.login',
          fetch_basic_profile: false });
      gapi.auth2.init({fetch_basic_profile: false,
          scope:'https://www.googleapis.com/auth/plus.login'}).then(
            function (){
              console.log('init');
              auth2 = gapi.auth2.getAuthInstance();
              auth2.isSignedIn.listen(updateSignIn);
              auth2.then(updateSignIn);
            });
    });
  });
}
