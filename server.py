#!/usr/bin/env python3

import os
import sys
import json
import requests
import re
import dateutil.parser
import multiprocessing
import time
import datetime as dt

from bottle import *

stderr = lambda x: sys.stderr.write(x)


BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))
STATIC = os.path.join(BASEPATH, "static")
CACHE = os.path.join(BASEPATH, "cache.txt")
DUMMY = False

try:
    CONFIG = json.loads(open(os.path.realpath(sys.argv[1]), "r").read())
except:
    print("Usage: python3 server.py <config json>")
    exit(1)

##############################################################################

TOKEN = multiprocessing.Array("c", 4096)

def loadToken():
    global TOKEN
    try:
        update = open("findtheklue-token", "r").read().encode("ascii")
        for i in range(0, len(TOKEN)):
            TOKEN[i] = update[i] if i < len(update) else b"\x00"
        print("Token set to: %s" % update)
    except Exception as e:
        print(e)
        return

def saveToken():
    t = getToken()
    open("findtheklue-token", "w+").write(t)

def setToken(tokenType, tokenStr):
    global TOKEN
    update = ("%s %s" % (tokenType, tokenStr)).encode("ascii")
    for i in range(0, len(TOKEN)):
        TOKEN[i] = update[i] if i < len(update) else b"\x00"
    print("Token set to: %s" % update)
    saveToken()

def getToken():
    global TOKEN
    token = b"".join(TOKEN).strip().rstrip(b"\x00").strip()
    token = token.decode("ascii")
    return token

def clearToken():
    global TOKEN
    for i in range(0, len(TOKEN)):
        TOKEN[i] = b"\x00"
    saveToken()

loadToken()

##############################################################################

class PlainMessage:
    def __init__(self, msg):
        self.method = "sendMessage"
        self.msg = {
            "text": msg,
            "parse_mode": "HTML",
        }

class PhotoMessage:
    def __init__(self, url):
        self.method = "sendPhoto"
        self.msg = {
            "photo": url,
        }

class LocationMessage:
    def __init__(self, lat, lng):
        self.method = "sendLocation"
        self.msg = {
            "latitude": float(lat),
            "longitude": float(lng),
        }
    

def publish(telegram, message, silent=False, dummy=False):
    url = "https://api.telegram.org/bot%s/%s" % (
        telegram["token"], message.method)
    msg = message.msg
    msg["chat_id"] = telegram["chat_id"]
    if silent:
        msg["disable_notification"] = True

    if not dummy:
        try:
            requests.post(url, json=msg, timeout=10)
        except Exception as e:
            print(e)
    else:
        print("############## DUMMY MODE")
        print(url)
        print(msg)
        print("\n")
    return

##############################################################################

send = lambda msg: \
    publish(CONFIG["telegram"], msg, silent=CONFIG["silent"], dummy=DUMMY)

sendInternalWarning = lambda msg: \
    publish(CONFIG["telegram"], PlainMessage(msg), silent=True, dummy=DUMMY)

def filterOutNewMessages(mids):
    global CACHE
    if type(mids) != list: mids = [mids]
    ret = []
    if not os.path.isfile(CACHE):
        open(CACHE, "w+").write('')
    try:
        with open(CACHE, 'r') as f:
            records = f.read()
            for mid in mids:
                if ("|%s" % mid) not in records:
                    ret.append(mid)
    except Exception as e:
        print(e)
        pass
    return ret

def markAsRead(mids):
    global CACHE
    if type(mids) != list: mids = [mids]
    with open(CACHE, 'a+') as f:
        for mid in mids:
            f.write("|%s" % mid)

def findLocations(data):
    string = json.dumps(data)
    feeds = set(re.findall("=(\-?[0-9\.]+)\,(\-?[0-9\.]+)", string))
    ret = []
    for each in feeds:
        try:
            lat, lng = float(each[0]), float(each[1])
            assert lat > -90.0 and lat < 90.0
            assert lng > -180.0 and lng < 180.0
        except:
            continue
        ret.append((lat, lng))

    return ret

def findImages(data):
    ret = []
    if type(data) == dict:
        isImage = False
        url = ""
        
        if "image" in data:
            isImage = True
            if "url" in data["image"]:
                url = data["image"]["url"]
        if isImage:
            if "fullImage" in data and "url" in data["fullImage"]:
                url = data["fullImage"]["url"]

        if isImage and url:
            return [url]
        else:
            for key in data:
                ret += findImages(data[key])

    elif type(data) == list:
        for each in data:
            ret += findImages(each)
    return list(set(ret))

# -------------------------------------------------------------------------- #

def handle(json):
    #print(json)
    print("---------------")

    mid = json["id"]
    url = json["url"]
    title = json["title"]
    payload = json["object"]

    content = payload["content"]

    try:
        attachments = payload["attachments"]
        images = findImages(attachments)
    except:
        images = []


    locations = findLocations(json)

    print("id=%s" % mid)
    print("title=%s" % title)

    # ---- proceed message

    if mid not in filterOutNewMessages([mid]):
        print("**** message ignored as it's known to us. ****")
        return

    textMessage = "<a href=\"%s\">%s</a>\n" % (url, title) + \
        "-----------------------\n"
    send(PlainMessage(textMessage))

    for url in images:
        send(PhotoMessage(url))

    for lat, lng in locations:
        send(LocationMessage(lat, lng))

    # ---- mark as read
    markAsRead([mid])


LASTUPDATE = multiprocessing.Value("f")

def search():
    global LASTUPDATE
    sendInternalWarning(
        "** I've got no clue of where Klue is! ^_^ **\n" +
        "UTC Time for this start: %s" % dt.datetime.utcnow()
    )

    lastLoopTime = time.time()

    while True:
        nowtime = time.time()
        stderr("==========> Last loop used time: %d seconds." % (nowtime - lastLoopTime))
        lastLoopTime = nowtime

        time.sleep(9)
        token = getToken()
            
        if not token: continue
        print("Initiate new search for Klue...")

        try:
            r = requests.get(
                "https://content.googleapis.com/plus/v1/activities?maxResults=20&query=%23NL1331", 
                headers={
                    "authorization": token,
                }
            )
            if r.status_code >= 400 and r.status_code < 500:
                print("TOKEN INVALID!", r.status_code)
                sendInternalWarning("** SERVER TOKEN FAILURE. RELOGIN TO FIX THIS. **")
                clearToken()
                continue

            if r.status_code != 200:
                print("FAILED RETRIEVING DATA: SERVER RETURNED %d" % r.status_code)
                continue

            data = json.loads(r.text)
            for item in data["items"]:
                handle(item)

            LASTUPDATE.value = time.time()
            print("Done.")

        except KeyboardInterrupt as e:
            break
        except Exception as e:
            print(e)
            continue


##############################################################################

@route("/")
def index():
    return redirect("/index.html")

@route("/<filepath:path>")
def staticfile(filepath):
    global STATIC
    return static_file(filepath, STATIC)

@route("/status")
def status():
    global LASTUPDATE, CONFIG
    return json.dumps({
        "lastUpdate": LASTUPDATE.value,
        "tokenValid": bool(getToken()),
        "silent": CONFIG["silent"],
    })

@post("/report")
def report():
    try:
        data = json.loads(request.body.read().decode('utf-8'))
    except:
        return
    handle(data)
    return "ok"

@post("/token")
def token():
    try:
        data = json.loads(request.body.read().decode("utf-8"))
        tokenType = data[0]
        tokenStr  = data[1]
    except:
        return
    setToken(tokenType, tokenStr)
    print("new token updated: %s %s...%s" % (
        tokenType, tokenStr[:10], tokenStr[-10:]))
    return "ok"


##############################################################################

p = multiprocessing.Process(target=search)
p.start()

run(host="127.0.0.1", port=1331)
